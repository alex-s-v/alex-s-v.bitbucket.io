function generateTemplate(x, y, w, W, H, N) {

    const d = (W - w * (N + 1)) / N;
    const D = d + w * 2;
    const R = D / 2;
    const r = d / 2;

    const Dw = D - w;

    if (x < 0 || y < 0 || w < 0 || W < 0 || H < 0 || N < 0 || d < 0 || D < 0 || R < 0 || r < 0 || Dw < 0) {
        throw new Error("Invalid arguments for template construction!");
    }

    const path = d3.path();

    path.moveTo(x, y);

    for (let i = 0; i < N; i++) {
        if (i % 2 == 0) {
            // Outter arch
            path.arc(x + Dw * i + R, y + H - R, R, Math.PI, Math.PI * 2, true);
        } else {
            // Inner arch
            path.arc(x + Dw * i + R, y + R, r, Math.PI, Math.PI * 2, false);
        }
    }

    // Move X to upper right corner
    const cxn = x + Dw * N + w;
    // Right straight segment
    path.lineTo(cxn, y);
    path.lineTo(cxn - w, y);

    for (let i = 0; i < N; i++) {
        if (i % 2 == 0) {
            // Inner arch
            path.arc(cxn - Dw * i - R, y + H - R, r, 0, Math.PI, false);
        } else {
            // Outter arch
            path.arc(cxn - Dw * i - R, y + R, R, 0, Math.PI, true);
        }
    }

    // Left straight segment
    path.lineTo(x + w, y);
    path.lineTo(x, y);

    path.closePath();

    return path;
}

function calculateTemplateSurface(w, W, H, N) {
    const d = (W - w * (N + 1)) / N;
    const D = d + w * 2;

    S_ach = Math.PI * (D ** 2 - d ** 2) / 8;
    S_str = w * (H - D);
    S_edg = w * (H - D / 2);
    return S_ach * N + S_str * (N - 1) + S_edg * 2;
}

class OptimizationError extends Error {
    constructor(message) {
        super(message);
        this.name = "OptimizationError";
    }
}

function simpleStepOptim(f, x0, x1, eps = 1e-5, maxiter = 500) {
    let f0 = f(x0);
    let f1 = f(x1);
    let dx = x1 - x0;
    for (let i = 0; i < maxiter; i++) {
        if (Math.abs(dx) < eps) return { x: x1, fx: f1 };
        if (f1 > f0) dx = -dx / 1.618;
        x0 = x1;
        f0 = f1;
        x1 = x0 + dx;
        f1 = f(x1);
    }
    throw new OptimizationError(`simpleStepOptim: Local minimum was not found in ${maxiter} steps!`);
}

function diff(a1, a2) {
    let s = 0;
    const N = a1.length;
    for (let i = 0; i < N; i++) {
        s += Math.abs(a1[i] - a2[i]);
    }
    return s / N;
}

function minimize(f, x, eps = 1e-4, maxiter = 5000) {
    let prevDiff = 0;
    for (let i = 0; i < maxiter; i++) {
        x1 = x.slice();
        for (let j = 0; j < x.length; j++) {
            const optf = (x) => {
                x1[j] = x;
                return f(x1);
            };

            const res = simpleStepOptim(optf, x[j], x[j] + x[j] * 0.2);
            x1[j] = res.x;
        }
        prevDiff = diff(x, x1);
        if (prevDiff < eps) return { x: x1, fx: f(x) };
        x = x1.slice();
    }
    throw new OptimizationError(`minimize: Local minimum was not found in ${maxiter} steps! ${prevDiff}`);
}

const round = (x, n) => Math.round(x * 10 ** n) / 10 ** n;

const input_w = document.querySelector("#ch-width");
const input_W = document.querySelector("#width");
const input_H = document.querySelector("#height");
const input_N = document.querySelector("#num");
const input_S = document.querySelector("#surf");
const input_P = document.querySelector("#path");
const input_F = document.querySelector("#filename");
input_F.value = "template.svg";

const getParameters = () => ({
    w: input_w.valueAsNumber,
    W: input_W.valueAsNumber,
    H: input_H.valueAsNumber,
    N: input_N.valueAsNumber,
    S: input_S.valueAsNumber
});

const setParameters = (p) => {
    input_w.value = round(p.w, 2);
    input_w.previousElementSibling.classList.remove("active");
    input_w.previousElementSibling.classList.add("active");

    input_W.value = round(p.W, 2);
    input_W.previousElementSibling.classList.remove("active");
    input_W.previousElementSibling.classList.add("active");

    input_H.value = round(p.H, 2);
    input_H.previousElementSibling.classList.remove("active");
    input_H.previousElementSibling.classList.add("active");

    input_N.value = round(p.N, 2);
    input_N.previousElementSibling.classList.remove("active");
    input_N.previousElementSibling.classList.add("active");

    input_S.value = round(p.S, 2);
    input_S.previousElementSibling.classList.remove("active");
    input_S.previousElementSibling.classList.add("active");

    input_P.value = round(p.S / p.w, 2);
    input_P.previousElementSibling.classList.remove("active");
    input_P.previousElementSibling.classList.add("active");
};

// 2.54 -> cm in 1 inch
// 10 -> mm in 1 cm
const scale = document.getElementById("dpi").offsetHeight / 2.54 / 10;

const defaults = {
    w: 2,
    W: 30,
    H: 30,
    N: 5,
    S: 400
};

const optf = (x) => (calculateTemplateSurface(x[0], defaults.W, x[1], defaults.N) - defaults.S) ** 2;
const res = minimize(optf, [defaults.w, defaults.H]);
defaults.w = res.x[0];
defaults.H = res.x[1];
defaults.S = calculateTemplateSurface(defaults.w, defaults.W, defaults.H, defaults.N);

class Template {
    constructor(x, y, p = defaults) {
        this.x = x;
        this.y = y;
        this.props = p;
    }

    update() {
        try {
            this.path = generateTemplate(this.x, this.y, this.p.w, this.p.W, this.p.H, this.p.N);
        } catch (err) {
            alert(err.message);
            return 0;
        }
        this.p.S = calculateTemplateSurface(this.p.w, this.p.W, this.p.H, this.p.N);
    }

    set props(p) {
        this.p = p;
        this.update();
    }

    get props() {
        return this.p;
    }

    toString() {
        return this.path.toString();
    }
}

let templates = [new Template(10, 10)];

function maxHeight() {
    let mh = 0;
    for (let i = templates.length - 1; i >= 0; i--) {
        const t = templates[i];
        if (t.p.H > mh)
            mh = t.p.H;
        // Consider only one row (same y position)
        if (i > 0 && t.y != templates[i - 1].y)
            break;
    }
    return mh;
}

setParameters(defaults);
update(templates);

function update(templates) {
    d3.select("#template").selectAll("*").remove();
    d3.select("#template")
        .append("path")
        .attr("d", templates.join(""))
        .attr("transform", `scale(${scale})`)
        .attr("fill", "black");
}

document.querySelector("#add").addEventListener("click", () => {
    const pt = templates[templates.length - 1];
    const nt = new Template(pt.x + pt.p.W + 10, pt.y);

    if (nt.x + nt.p.W + 5 > 210) {
        const mh = maxHeight();
        nt.y = pt.y + mh + 10;
        nt.x = 10;
        nt.update();
    }

    templates.push(nt);
    setParameters(nt.props);
    update(templates);
});

// TODO: Shift template to the next line if it goes off the page when recalculated
// TODO: Alert if optimization failed
// FIXME: Optimization with multiple variables works strange (for example w and H for S = 200)
document.querySelector("#calculate").addEventListener("click", () => {
    const p = getParameters();

    if (!isNaN(p.S)) {
        const names = ["w", "W", "H"];
        let opt_names = [];
        let x0 = [];
        for (let i = 0; i < names.length; i++) {
            if (isNaN(p[names[i]])) {
                x0.push(defaults[names[i]]);
                opt_names.push(names[i]);
            }
        }

        if (x0.length > 0) {

            // console.log(x0);
            // console.log(opt_names);

            function optf(x) {
                for (let i = 0; i < x.length; i++) {
                    p[opt_names[i]] = x[i];
                }
                return (calculateTemplateSurface(p.w, p.W, p.H, p.N) - p.S) ** 2;
            }
            let res;
            try {
                res = minimize(optf, x0);
            } catch(err) {
                if (err instanceof OptimizationError) {
                    alert(`Optimization failed with the error:\n${err.message}`);
                }
                return 0;
            }
            for (let i = 0; i < res.x.length; i++) {
                p[opt_names[i]] = res.x[i];
            }
        }
    }

    templates[templates.length - 1].props = p;
    setParameters(templates[templates.length - 1].props);
    update(templates);
});

document.querySelector("#clear").addEventListener("click", () => {
    templates = [new Template(10, 10)];
    update(templates);
});


// Scaling the SVG element to fit the page
function resizeSVG() {
    const svg = document.querySelector("#template");
    const scale = (window.innerHeight * 0.9) / svg.height.baseVal.value;
    svg.style.transform = `scale(${scale})`;
}

resizeSVG();
window.addEventListener("resize", resizeSVG);

// Saving the generated SVG
document.querySelector("#download").addEventListener("click", () => {
    const svgData = document.querySelector("#template").outerHTML;
    const svgBlob = new Blob([svgData], { type: "image/svg+xml;charset=utf-8" });
    const svgUrl = URL.createObjectURL(svgBlob);
    const downloadLink = document.createElement("a");
    downloadLink.href = svgUrl;
    downloadLink.download = input_F.value || "template.svg";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
});