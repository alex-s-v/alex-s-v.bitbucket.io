const fileInput = document.getElementById("file-input");
let data = null;
let text;

fileInput.addEventListener("change", (event) => {
    const files = event.target.files;

    const reader = new FileReader();
    reader.addEventListener("load", (event) => {
        const result = convertGCode(event.target.result);
        download("rel_" + files[0].name, result);
    });
    reader.readAsText(files[0]);
});

/**
 * @param {String} code GCode
 */
function convertGCode(code) {
    let lines = code
        .trim()
        .replaceAll("M106 P1\r\n", "")
        .replaceAll("P1 ", "")
        .split("\n");
    lines.splice(lines.length - 1, 0, lines[15]);  // Return to starting position

    const re1 = /G(\d) X([\-\+]?\d+(?:\.\d+)?) Y([\-\+]?\d+(?:\.\d+)?)\s?(F\d+)?/;
    const re2 = /M107/;
    const re3 = /M106 S([\-\+]?\d+(?:\.\d+)?)/;
    let prev_pos = [0, 0];
    let new_lines = [];
    lines.forEach(l => {
        l = l.trim();
        const match = l.match(re1);
        if (match) {
            let [g, x, y, f] = match.slice(1);
            // Recalculate coordinates to relative mode
            const dx = Math.round((prev_pos[0] - x) * 100) / 100;
            const dy = Math.round((prev_pos[1] - y) * 100) / 100;
            const new_l = `G${g} X${dx} Y${dy}${f ? " "+f : ""}`;
            new_lines.push(new_l);
            prev_pos = [x, y];
        } else {
            const match1 = l.match(re2);
            const match2 = l.match(re3);
            new_lines.push(l);
            if (match1)  // Add pause after lazer off to let it completely shut down
                new_lines.push("G04 S2");
            else if (match2)  // Add pause after lazer on to let it gain full power
                new_lines.push("G04 S1");
        }
    });

    new_lines.splice(3, 0, "G91");  // Relative coordinates mode
    new_lines.splice(16, 1);  // Remove first G0 command
    new_lines.splice(1, 2);  // Remove absolute coordinates mode and Z positioning

    // Repeat a few steps of the actual channel cutting to get rid of the skiped segment
    let repeated = new_lines.slice(28, 58);
    new_lines.splice(new_lines.length-4, 0, "", ...repeated, "");
    repeated.forEach((l, i) => {
        const match = l.match(re1);
        if (!match)
            console.log(l);
        let [g, x, y, f] = match.slice(1);
        repeated[i] = `G${g} X${-x} Y${-y}`;
    });
    // Go back
    new_lines.splice(new_lines.length-2, 0, "", ...repeated.reverse(), "");

    return new_lines.join("\n");
}

function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/x.gcode;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}